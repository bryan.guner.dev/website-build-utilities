
# Project name

<!--- These are examples. See https://shields.io for others or to customize this set of shields. You might want to include dependencies, project status and licence info here --->

[![GitHub issues](https://img.shields.io/github/issues/bgoonz/website-build-utilities)](https://github.com/bgoonz/website-build-utilities/issues)
![GitHub forks:](https://img.shields.io/github/forks/bgoonz/website-build-utilities)
[![GitHub stars](https://img.shields.io/github/stars/bgoonz/website-build-utilities)](https://github.com/bgoonz/website-build-utilities/stargazers)
[![GitHub license](https://img.shields.io/github/license/bgoonz/website-build-utilities)](https://github.com/bgoonz/website-build-utilities/blob/master/LICENSE)
![Twitter Follow](https://img.shields.io/twitter/follow/bgoonz?style=social)






This project is a `set of file system utilities geared twards simple static website tidyness and orginization` that allows `budding developers ` to `easily turn a directory of notes comprised of various file extensions and convert them into a cohesive website experience`.



## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed node 12.4.0 or later`
* You have a `<Windows/Linux/Mac>` machine. 
* You have read `<guide/link/documentation_related_to_project>`.

## Installing <project_name>

To install <project_name>, follow these steps:

Linux and macOS:
```
<install_command>
```

Windows:
```
<install_command>
```
## Using <project_name>

To use <project_name>, follow these steps:

```
<usage_example>
```

Add run commands and examples you think users will find useful. Provide an options reference for bonus points!

## Contributing to <project_name>
<!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
To contribute to <project_name>, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

* [@bgoonz](https://github.com/bgoonz) 📖
* [@cainwatson](https://github.com/cainwatson) 🐛
* [@calchuchesta](https://github.com/calchuchesta) 🐛

You might want to consider using something like the [All Contributors](https://github.com/all-contributors/all-contributors) specification and its [emoji key](https://allcontributors.org/docs/en/emoji-key).

## Contact

If you want to contact me you can reach me at <your_email@address.com>.

## License
<!--- If you're not sure which open license to use see https://choosealicense.com/--->

This project uses the following license: [<license_name>](<link>).




# :package: NPM Package Template :package:

Minimal tested and fully functional NPM package template project for ES6+ Javascript.

## About this package

This template is intended to allow you to **start developing immediatly** with a working set
of tools and scripts that play well together.

It is little opinionated to the point, that only the most basic tools are integrated.
If you want a different flavour you can fork this project and easily replace the tools or add new ones.

## Which tools are used?

All tools are only defined as **`dev-dependencies`**:

* [Babel](https://babeljs.io/) for transpiling ES6+ to ES5 plus minification, sourcemaps etc.
* [Mocha](https://mochajs.org/) and [Chai](https://www.chaijs.com) for testing
* [Istanbul](https://istanbul.js.org/) for code coverage
* [Standard](https://standardjs.com/) for linting
* [JSDoc](https://jsdoc.app/) for documentation and [jsdoc-to-markdown](https://www.npmjs.com/package/jsdoc-to-markdown) to create docs as markdown files
* [Travis-CI](https://travis-ci.org/) for continuous integration
* [GitHub actions](https://github.com/features/actions) for continuous integration


## Getting started

Just create your Js files in the `./lib` folder and add the tests in the `test` folder.


## Code Quality

We use `standard` as opinionated but zero-config linter.
You can run lint in two modes:

##### lint 
 
```bash
$ npm run lint
``` 

##### lint with auto fixing

```bash
$ npm run lint:fix
``` 

## Run the tests

We use mocha and chai with a mostly (but not strict) behavioural style for testing.
You can run the tests in three different contexts:

##### Single run

```bash
$ npm run test
``` 

##### Watch mode

```bash
$ npm run test:watch
``` 

##### Coverage

```bash
$ npm run test:coverage
``` 

## Documentation

Documentation is using jsDoc and is available as [html](docs/index.html) or [markdown](api.md) version.

To build the documentation in development, you need to run 

```bash
$ npm run docs
``` 

## License

MIT, see [license file](LICENSE)
