import fs from 'fs';
import request from 'request';
import cheerio from 'cheerio';
import { forEach } from 'helper-tools/src/object';
import State from './State';
import Kernel from './Kernel';

/**
 * Aclaraciones de desarrollo, por ej: "@todo muchas responsabilidades, abstraer"
 */
export class Scraper {

    /**
     * Comentarios sobre qué propiedades recibe el constuctor como parámetos.
     */
    constructor(props) {
        console.log('Scraper constructor');
        this.setInitialState(props);
        this.loadKernel(Kernel);
    }
    
    setInitialState(props) {
        this.state = new State(props);
    }

    loadKernel(Kernel) {
        const kernel = new Kernel({
            requestHandlers: {
                '/': this.onScrapRequest
            }
        });

        this.kernel = kernel;

        return kernel;
    }

    scrap(url, extractors) {
        this.setDataExtractors(extractors);
        return this.requestHtml(url);
    }

    onScrapRequest(request, response) {
        this.lastScrapRequest = { request, response };

        return this.requestHtml(url);
    }

    requestHtml(url) {
        return request(url, this.onHtmlReady.bind(this));
    }

    onHtmlReady(error, response, html) {
        const hasError = error;
        
        if (hasError) {
            this.htmlRequestErrorHandler(error, { req, res, response, html });
        } else {
            //Load html
            this.loadCurrentHtml(html);

            //extract data;
            const extractedData = this.extractData();
            
            //Parse data
            const parsedData = this.parseData(extractedData);
            
            //Store into database / file
            this.storeData(parsedData);
            
            //User notification
            this.notifyUser('Éxito, guardando los datos...');
        }
    }

    parseData(data) {
        const parsedData = data;
        
        return parsedData;
    }

    storeData(data) {
        //console.log('storeData', { data });
        fs.writeFile('other-scraper_database.json', JSON.stringify(data, null, 4), (error) => {
            if (error) {
                console.error('Hubo un error', { error })
            } else {
                this.notifyUser('Exito, other-scraper_database.json se creado/actualizado.');
            }
        })
    }

    notifyUser(message) {
        console.log(message);
    }

    htmlRequestErrorHandler(error, data) {
        console.log('htmlRequestErrorHandler', { error, data });
    }

    loadCurrentHtml(html) {
        this.currentHtml = cheerio.load(html);
    }

    extractData() {
        const $ = this.currentHtml;
        let extractedData = [];

        forEach(this.dataExtractors, (extractor, title) => {
            extractedData.push({
                title,
                data: extractor($)
            });
        });

        return extractedData;
    }

    setDataExtractors(extractors) {
        this.dataExtractors = extractors;
    }
}

export default Scraper;
