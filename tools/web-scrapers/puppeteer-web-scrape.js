const puppeteer = require('puppeteer')

class WebScraper {
  constructor () {
  }

  async start (options) {
    this._browser = await puppeteer.launch(options)
    this._page = await this._browser.newPage()
    return this
  }

  async goToPage(url) {
    await this._page.goto(url)
    return this
  }

  async evaluate (pageFunction, selector) {
    return await this._page.evaluate(pageFunction, selector)
  }

  async $ (selector) {
    return await this._page.$(selector)
  }

  async $$ (selector) {
    return await this._page.$$(selector)
  }

  async $eval(selector, pageFunction) {
    return await this._page.$eval(selector, pageFunction)
  }

  close() {
    return this._browser.close()
  }
}

module.exports = WebScraper
