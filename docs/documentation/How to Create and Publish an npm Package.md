# How to Create and Publish an npm Package

> Easily create and publish an npm module to npm repository

[

![Yogesh Chavan](https://miro.medium.com/fit/c/96/96/2*pyD2GzDw85mamTF-UzZk_w.png)



](https://yogeshchavan.medium.com/?source=post_page-----17b5e1744f26--------------------------------)

Easily create and publish an npm module to npm repository

![Image for post](https://miro.medium.com/max/60/1*eFzpK8EZGLttHMKLgNM1eA.png?q=20)

![Image for post](https://miro.medium.com/max/4740/1*eFzpK8EZGLttHMKLgNM1eA.png)

In this tutorial, you will create your own npm package and publish it to the npm repository.

By doing this, you will understand:

1.  How to create an npm package
2.  How to install it locally before publishing to test its functionality
3.  How to install and use the published package using ES6 import syntax or using Node.js require statement
4.  How to manage semantic versioning of the package
5.  How to update the package with the new version and publish it again

_To be precise, you will build a package that will return a list of GitHub repositories of the specified username sorted by the number of stars for each repository._

You will need the following to complete this tutorial:

*   A valid installation of Git version control
*   Node.js installed locally, which you can do by following the instructions given on [this page](https://nodejs.org/en/download/)

This tutorial was verified with Node v13.14.0, npm v6.14.4, and axios v0.20.0

Create a new folder with the name `github-repos-search` and initialize a `package.json` file

mkdir github-repos-search  
cd github-repos-search  
npm init -y

Initialize the current project as a git repository by running the following command from `github-repos-search` folder:

git init .

Create a `.gitignore` file to exclude the `node_modules` folder. Add the following contents inside `.gitignore` file

node\_modules

Install the `axios` package that you will use to make a call to the GitHub API.

npm install axios@0.20.0

Your `package.json` will look like this now:

{  
  "name": "github-repos-search",  
  "version": "1.0.0",  
  "description": "",  
  "main": "index.js",  
  "dependencies": {  
    "axios": "^0.20.0"  
  },  
  "devDependencies": {},  
  "scripts": {  
    "test": "echo \\"Error: no test specified\\" && exit 1"  
  },  
  "keywords": \[\],  
  "author": "",  
  "license": "ISC"  
}

> _Inside the_ `_package.json_` _file, the value for name is_ `_github-repos-search_`_. So our package name after publishing to npm repository will become_ `_github-repos-search_`_. Also, the name has to be unique on the npm repository so first check if such npm repository already exists or not by navigating to_ `_https://www.npmjs.com/package/<your_repository_name_from_package_json>_`_. Otherwise you will get an error while publishing the package to the npm repository if the name already exists._

Create a new file with the name `index.js` and add the following contents inside it:

const axios = require('axios');  
const getRepos = async ({  
  username = 'myogeshchavan97',  
  page = 1,  
  per\_page = 30  
} = {}) => {  
  try {  
    const repos = await axios.get(  
      \`https://api.github.com/users/${username}/repos?page=${page}&per\_page=${per\_page}&sort=updated\`  
    );  
    return repos.data  
      .map((repo) => {  
        return {  
          name: repo.name,  
          url: repo.html\_url,  
          description: repo.description,  
          stars: repo.stargazers\_count  
        };  
      })  
      .sort((first, second) => second.stars - first.stars);  
  } catch (error) {  
    return \[\];  
  }  
};getRepos().then((repositories) => console.log(repositories));

Let’s understand the code first.

*   You have created a `getRepos` function that accepts an optional object with `username`, `page` and `per_page` properties.
*   Then you used object destructuring syntax for getting those properties out of the object.
*   Passing an object to the function is optional so we have initialized it to default values if the object is not passed to the function like this:

{  
  username = 'myogeshchavan97',  
  page = 1,  
  per\_page = 30  
} = {}

*   The reason for assigning an empty object `{}` is to not get an error while destructuring `username` from the object if the object is not passed. Check out my [previous article](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/javascript-in-plain-english/what-is-so-special-about-es6-destructuring-6be45fbec52c?source=friends_link&sk=6b8ace6e0db36ec4f5b525ef19c62a41) to learn about destructuring in detail.
*   Then inside the function, you are making a call to the GitHub API by passing the required parameters to get the repositories of the specified user sorted by the updated date.

const repos = await axios.get(  
      \`https://api.github.com/users/${username}/repos?page=${page}&per\_page=${per\_page}&sort=updated\`  
    );

*   Here, you are using async/await syntax so the `getRepos` function is declared as async.
*   Then you are selecting only the required fields from the response using the Array `map` method

repos.data  
  .map((repo) => {  
    return {  
      name: repo.name,  
      url: repo.html\_url,  
      description: repo.description,  
      stars: repo.stargazers\_count  
    };  
  })

Then that result is sorted by descending order of stars so the first element in the list will be with the highest stars

.sort((first, second) => second.stars - first.stars);

*   If there is any error, you are returning an empty array in the catch block.
*   As the `getRepos` function is declared as `async`, you will get back a promise so you are using `.then` handler to get the result of the `getRepos` function call and printing to the console.

getRepos().then((repositories) => console.log(repositories));

Now, run the index.js file by executing the following command from the command line:

node index.js

You will see the following output with the first 30 repositories:

![Image for post](https://miro.medium.com/max/3200/1*6mbU4OCi8dt98pU_HPEZZg.png)

In the file, you have not provided the username so by default my repositories are displayed.  
Let’s change that to the following code:

getRepos({  
  username: 'gaearon'  
}).then((repositories) => console.log(repositories));

Run the file again by executing `node index.js` command and you will see the following output:

![Image for post](https://miro.medium.com/max/3200/1*HkEx6P6Dwodjax6TmecbrA.png)

You can choose to pass the `page` and `per_page` properties to change the response to get the first 50 repositories.

getRepos({  
  username: 'gaearon',  
  page: 1,  
  per\_page: 50  
}).then((repositories) => console.log(repositories));

Now, you know that the functionality is working. Let’s export this module so you can call this `getRepos` method from any other file.

So remove the below code from the file

getRepos({  
  username: 'gaearon',  
  page: 1,  
  per\_page: 50  
}).then((repositories) => console.log(repositories));

and add the below line instead

module.exports = { getRepos };

Here, you are exporting the `getRepos` function as a property of the object so later if you want to export any other function you can easily add it to the object.

So the above line is the same as

module.exports = { getRepos: getRepos };

Now, you are done with creating the npm package but before publishing it to the npm repository, you need to make sure it works when you use it using `require` or `import` statement.

There is an easy way to check that. Execute the following command from the command line from inside the `github-repos-search` folder:

npm link

![Image for post](https://miro.medium.com/max/3200/1*K1Xt6e-v93AEN3pkVbZkpg.png)

Executing `npm link` command creates a symbolic link for your current package inside the global npm `node_modules` folder (The same folder where our global npm dependencies get installed)

So now you can use your created npm package inside any project.

Now, create a new folder on your desktop with any name for example `test-repos-library-node` and initialize a `package.json` file so you can confirm that the package is installed correctly:

cd ~/Desktop  
mkdir test-repos-library-node  
cd test-repos-library-node  
npm init -y

If you remember, the name property in our package’s `package.json` file was `github-repos-search` so you need to require the package using the same name.

Now, execute the following command from inside the `test-repos-library-node` folder to use the package you created:

npm link github-repos-search

![Image for post](https://miro.medium.com/max/3200/1*TXgK3QCOiOrIcPsQpx-rhQ.png)

Create a new file with the name `index.js` and add the following code inside it:

const { getRepos } = require('github-repos-search');getRepos().then((repositories) => console.log(repositories));

Here, you have imported the package directly from the `node_modules` folder( This was only possible because you linked it using npm link)

Now, run the file by executing it from the command line:

node index.js

You will see the correct output displayed:

![Image for post](https://miro.medium.com/max/3200/1*q_rDqicht4zdLV8fe-MhqQ.png)

> _This proves that when you publish the npm package on the npm repository, anyone can use it by installing it and using the require statement._

You have verified that the package works by using the require statement. Let’s verify it by using the ES6 import statement.

Create a new React project by executing the following command from your desktop folder:

cd ~/Desktop  
npx create-react-app test-repos-library-react

Now, execute the following command from inside the `test-repos-library-react` folder to use the package you created:

npm link github-repos-search

Now, open `src/App.s` file and replace it with the following content:

import { getRepos } from 'github-repos-search';  
import React from 'react';  
import './App.css';function App() {  
  getRepos().then((repositories) => console.log(repositories)); return (  
    <div className="App">  
      <h2>Open browser console to see the output.</h2>  
    </div>  
  );  
}  
export default App;

Start the React app by executing the following command from the terminal:

yarn start

If you check the browser console, you will see the output as expected:

![Image for post](https://miro.medium.com/max/3200/1*h4bWHGE8q_POxERqvpyFvw.png)

> _This proves that when you publish the npm package on npm repository, anyone can use it by installing it and using import statement._

Now, you have verified that the package is working fine.  
It’s time to publish it to the npm repository.

Switch back to the `github-repos-search` project folder where you have created the npm package.

Let’s add some metadata in the `package.json` file to display some more information about the package

Here is the final `package.json` file:

{  
  "name": "github-repos-search",  
  "version": "1.0.0",  
  "description": "",  
  "main": "index.js",  
  "homepage": "https://github.com/myogeshchavan97/github-repos-search",  
  "repository": {  
    "type": "git",  
    "url": "git+https://github.com/myogeshchavan97/github-repos-search.git"  
  },  
  "dependencies": {  
    "axios": "^0.20.0"  
  },  
  "devDependencies": {},  
  "scripts": {  
    "test": "echo \\"Error: no test specified\\" && exit 1"  
  },  
  "keywords": \[  
    "github",  
    "repos",  
    "repositories",  
    "sort",  
    "stars"  
  \],  
  "author": "Yogesh Chavan <myogeshchavan97@gmail.com>",  
  "license": "ISC"  
}

You have added `homepage`, `repository`, `keywords` and `author` for more information(These are optional). Make changes as per your GitHub repository.

Create a new GitHub repository [HERE](https://github.com/new) and push `github-repos-search` repository to GitHub. Navigate to [https://www.npmjs.com/](https://www.npmjs.com/) and create a new account If you don’t already have an account.

Open the terminal and from inside the `github-repos-search` folder, execute the following command:

npm login

and enter your npm credentials to log in.

![Image for post](https://miro.medium.com/max/3200/1*XH6K0Yb57FrE6rFUoMNtZA.png)

Now, to publish it to the npm repository run the following command:

npm publish

![Image for post](https://miro.medium.com/max/3200/1*7nqkMY32Fc0cUPX0WWD4_A.png)

If you navigate to [https://www.npmjs.com/package/github-repos-search](https://www.npmjs.com/package/github-repos-search) in the browser, you will see your published package:

![Image for post](https://miro.medium.com/max/3200/1*E7qtHDgALm47TWJUjbAxnw.png)

Now, let’s add a `readme.md` file for displaying some information regarding the package.

Create a new file with the name `readme.md` inside the `github-repos-search` folder with the contents from [here](https://raw.githubusercontent.com/myogeshchavan97/github-repos-search/master/readme.md)

Let’s try to publish it again using the npm publish command.

![Image for post](https://miro.medium.com/max/3200/1*fYqyPIq3XEFEeRE80mCwvg.png)

You will get an above error. This is because you are publishing the module with the same version again.

If you check our `package.json` file, you will see that the version mentioned in the file is `1.0.0`You need to increment it every time publishing a new change. So what should you increment to? For that, you need to understand the semantic versioning concept.

The version value is a combination of 3 digits separated by `dot` operator. Let’s say the version is `a.b.c`

1.  First value (`a` in `a.b.c`) specifies the major version of the package — It means this version has Major code changes and it might contain breaking API changes.
2.  Second value (`b` in `a.b.c`) specifies the minor version which contains minor changes but will not contain breaking API changes.
3.  Third value (`c` in `a.b.c`) specifies the patch version which usually contains bug fixes.

In our case, you just added a `readme.md` file which is not an API change so you can increment the patch version which is the last digit by 1.

So change the version inside `package.json` file from `1.0.0` to `1.0.1` and run the `npm publish` command again.

![Image for post](https://miro.medium.com/max/3200/1*F2kJFnxTDrYuCyZAhrUieQ.png)

If you check the npm package now, you will see the updated npm package live [here](https://www.npmjs.com/package/github-repos-search)

![Image for post](https://miro.medium.com/max/3200/1*gRm-Cy5cKgZ2tlzDk4Fjew.png)

To learn in detail about `semantic versioning` check out my [previous article](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/javascript-in-plain-english/semantic-versioning-in-npm-54d5b02c3536?source=friends_link&sk=e1c56eee8295d198ad986a8c3849d16a)

In this tutorial, you created an npm package and published it to the npm repository.

For the complete source code of this tutorial, check out the [github-repos-search](https://github.com/myogeshchavan97/github-repos-search) repository on GitHub. You can also see the published npm module [here](https://www.npmjs.com/package/github-repos-search)

**Don’t forget to subscribe to get my weekly newsletter with amazing tips, tricks and articles directly in your inbox** [**here.**](https://yogeshchavan.dev/)


[Source](https://medium.com/swlh/how-to-create-and-publish-an-npm-package-17b5e1744f26)