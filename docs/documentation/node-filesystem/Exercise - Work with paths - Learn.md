# Exercise - Work with paths - Learn

> Exercise - Work with paths

*   5 minutes

The Node.js _path_ module and `__dirname` global variable are two ways to define and compose file system paths.

In the previous exercise, you wrote a program that iterates through a folder to find any of the _sales.json_ files inside.

In this exercise, you'll use the _path_ module and `__dirname` global variable to improve the program so that it will find any file with a .json extension.

Include the path module
-----------------------

At the top of the _index.js_ file, include the _path_ module.

    const path = require("path");
    

Use the current directory
-------------------------

In the current index.js code, you're passing the static location of the _stores_ folder. We'll change that code to use the `__dirname` value instead of passing a static folder name.

1.  In the `main` method, create a variable to store a path to the _stores_ directory by using the `__dirname` constant.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
        
          const salesFiles = await findSalesFiles(salesDir);
          console.log(salesFiles);
        }
        
    
2.  Select the Ctrl / Cmd + S keys to save the file.
    
3.  Run the program from the command line.
    
        node index.js
        
    
    Notice that the path that's now listed for the files is the full system path. This is because the `__dirname` constant returns the full path to the current location.
    
        [
          '/home/username/node-files/stores/201/sales.json',
          '/home/username/node-files/stores/202/sales.json',
          '/home/username/node-files/stores/203/sales.json',
          '/home/username/node-files/stores/204/sales.json',
        ]
        
    

Join paths
----------

Instead of concatenating folder names to make a new path to search, you'll change the code to use the `path.join` method. This code will then work across operating systems.

1.  Change the `findFiles` method to use `path.join`.
    
        
        await findFiles(path.join(folderName, item.name));
        
    
2.  Select the Ctrl / Cmd + S keys to save the file.
    
3.  Run the program from the command line.
    
        node index.js
        
    
    The output will be the same as the previous step, but the program is now more robust because it uses `path.join` instead of concatenating strings.
    

Find all .json files
--------------------

Instead of looking for just _sales.json_ files, the program needs to search for any file with an extension of .json. To do that, use the `path.extname` method to check the file name extension.

1.  Run the following command in the terminal to rename the _stores/201/sales.json_ file to _stores/sales/totals.json_.
    
        mv stores/201/sales.json stores/201/totals.json
        
    
2.  In the `findFiles` method, change the `if` statement to check just the file name extension. Use the `path.join` method to compose the full path to the file.
    
        if (path.extname(item.name) === ".json") {
          
          await salesFiles.push(path.join(folderName, item.name));
        }
        
    
3.  Select the Ctrl / Cmd + S keys to save the file.
    
4.  Run the program from the command line.
    
        node index.js
        
    
    The output now shows all the .json files in any of the store ID directories.
    
        [
           '/home/username/node-files/stores/201/totals.json',
           '/home/username/node-files/stores/202/sales.json',
           '/home/username/node-files/stores/203/sales.json',
           '/home/username/node-files/stores/204/sales.json'
        ]
        
    

Great job! You've used the _path_ module and the `__dirname` constant to make the program much more robust. In the next section, you'll learn how to create directories and move files between locations.

### Got stuck?

If you got stuck at any point in this exercise, here's the completed code. Remove everything in _index.js_ and replace it with this solution.

    const fs = require("fs").promises;
    const path = require("path");
    
    async function findSalesFiles(folderName) {
      
      let salesFiles = [];
    
      async function findFiles(folderName) {
        
        const items = await fs.readdir(folderName, { withFileTypes: true });
    
        
        for (item of items) {
          
          if (item.isDirectory()) {
            
            await findFiles(path.join(folderName, item.name));
          } else {
            
            if (path.extname(item.name) === ".json") {
              
              salesFiles.push(path.join(folderName, item.name));
            }
          }
        }
      }
    
      await findFiles(folderName);
    
      return salesFiles;
    }
    
    async function main() {
      const salesDir = path.join(__dirname, "stores");
    
      
      const salesFiles = await findSalesFiles(salesDir);
      console.log(salesFiles);
    }
    
    main();
    

* * *

Next unit: Create files and directories
---------------------------------------

[Continue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/modules/nodejs-files/6-create-files-directories/)

Need help? See our [troubleshooting guide](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.5-exercise-paths&documentId=4928b075-f3e2-691a-f0c6-4bec8ea9aeaa&versionIndependentDocumentId=715423ac-d693-9675-11fe-167de88be0e9&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F5-exercise-paths.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F5-exercise-paths&author=buhollan) or provide specific feedback by [reporting an issue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.5-exercise-paths&documentId=4928b075-f3e2-691a-f0c6-4bec8ea9aeaa&versionIndependentDocumentId=715423ac-d693-9675-11fe-167de88be0e9&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F5-exercise-paths.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F5-exercise-paths&author=buhollan#report-feedback).


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/5-exercise-paths)