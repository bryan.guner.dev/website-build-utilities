# Create files and directories - Learn

> Create files and directories

*   4 minutes

Creating and deleting new files and directories programmatically is a common requirement for line-of-business applications.

So far, you've learned how to work with files and directories by using the _fs_ module. You can also use the _fs_ module to create, delete, copy, move, and otherwise manipulate files and directories on a system programmatically.

Here, you'll learn how to use the _fs_ module to create directories and files.

Create directories
------------------

The `mkdir` method allows creation of directories. The following method creates a folder called _newDir_ inside the _201_ folder.

    const fs = require("fs").promises;
    const path = require("path");
    
    await fs.mkdir(path.join(__dirname, "stores", "201", "newDir"));
    

Note that _/stores/201_ must already exist, or this method will fail. You can pass in an optional `recursive` flag if the file structure doesn't exist and you want the operation to create it.

    await fs.mkdir(path.join(__dirname, "newDir", "stores", "201", "newDir"), {
      recursive: true
    });
    

Make sure directories exist
---------------------------

If the directory that you're trying to create already exists, the `mkdir` method will throw an error. That's not good because that error will cause your program to stop abruptly. To avoid that situation, Node.js recommends that you wrap the `mkdir` method in a `try/catch` if you plan (as we do) to manipulate the file or directory after you open it.

    const pathToCreate = path.join(__dirname, "stores", "201", "newDirectory");
    
    
    try {
      await fs.mkdir(salesTotalsDir);
    } catch {
      console.log(`${salesTotalsDir} already exists.`);
    }
    

Create files
------------

You can create files by using the `fs.writeFile` method. This method takes in a path to the file, and the data that you want to write to the file. If the file already exists, it will be overwritten.

For instance, this code creates a file called _greeting.txt_ with the text "Hello World!" inside.

    await fs.writeFile(path.join(__dirname, "greeting.txt", "Hello World!"));
    

If you omit the third parameter, which is the data to be written to the file, Node.js will write "undefined" to the file. That's probably _not_ what you would ever want. To write an empty file, pass an empty string. An even better option would be to pass the `String` function, which effectively does the same thing without leaving empty quotes in your code.

    await fs.writeFile(path.join(__dirname, "greeting.txt", String()));
    

In the next exercise, you'll use your knowledge of how to create files and directories to extend the program to create a directory that will store the total of all sales files.

* * *

Next unit: Exercise - Create files and directories
--------------------------------------------------

[Continue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/modules/nodejs-files/7-exercise-create-files-directories/)


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/6-create-files-directories)