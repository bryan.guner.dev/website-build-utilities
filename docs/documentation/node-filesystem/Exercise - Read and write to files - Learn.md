# Exercise - Read and write to files - Learn

> Exercise - Read and write to files

*   5 minutes

You can also use the _fs_ module in Node.js to write data to files and read data from files.

You're almost finished creating a Node.js masterpiece for Tailwind Traders. So far, your code reads any directory, finds all .json files, and creates a _totals.txt_ file.

In this exercise, you'll complete the project by reading the .json files, adding up the store totals, and writing the grand total to the _totals.txt_ file.

Create a method to calculate sales totals
-----------------------------------------

1.  At the top of index.js, just below the `require("path")` statement, create a function that will calculate the sales total. This method should take in the array of file paths that it can iterate over.
    
        async function calculateSalesTotal(salesFiles) {
          let salesTotal = 0;
        
          
        
          return salesTotal;
        }
        
    
2.  Within that method, replace `// READ FILES LOOP` with a loop that iterates over the `salesFiles` array, reads the file, parses the content as JSON, and then increments the `salesTotal` variable with the `total` value from the file.
    
        async function calculateSalesTotal(salesFiles) {
          let salesTotal = 0;
          
          for (file of salesFiles) {
            
            const data = JSON.parse(await fs.readFile(file));
            
            salesTotal += data.total;
          }
          return salesTotal;
        }
        
    

Call the calculateSalesTotals method
------------------------------------

1.  In the `main` function, add a call to the `calculateSalesTotals` function just above the `fs.writeFile` call.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
          const salesTotalsDir = path.join(__dirname, "salesTotals");
        
          
          try {
            await fs.mkdir(salesTotalsDir);
          } catch {
            console.log(`${salesTotalsDir} already exists.`);
          }
        
          
          const salesFiles = await findSalesFiles(salesDir);
        
          
          const salesTotal = await calculateSalesTotal(salesFiles);
        
          
          await fs.writeFile(path.join(salesTotalsDir, "totals.txt"), String());
        }
        
    

Write the total to the totals.txt file
--------------------------------------

1.  In the `main` function, modify the `fs.writeFile` block to write the value of the `salesTotal` variable to the _totals.txt_ file.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
          const salesTotalsDir = path.join(__dirname, "salesTotals");
        
          
          try {
            await fs.mkdir(salesTotalsDir);
          } catch {
            console.log(`${salesTotalsDir} already exists.`);
          }
        
          
          const salesFiles = await findSalesFiles(salesDir);
        
          
          const salesTotal = await calculateSalesTotal(salesFiles);
        
          
          await fs.writeFile(
            path.join(salesTotalsDir, "totals.txt"),
            `${salesTotal}\r\n`,
            { flag: "a" }
          );
        }
        
    
2.  Select the Cmd / Ctrl + S keys to save the _index.js_ file.
    

Run the program
---------------

1.  Run the program from the terminal.
    
        node index.js
        
    
    There is no output from the program. If you look in the _salesTotals/totals.txt_ file, you'll see the total of all the sales from the _sales.json_ file.
    
2.  Run the program from the terminal again.
    
        node.index.js
        
    
3.  Select the _index.js_ file.
    
4.  Select the _salesTotals/totals.txt_ file.
    
    The _totals.txt_ file now has a second line. Every time you run the program, the totals are added up again and a new line is written to the file.
    

Outstanding work! You've written a smart, robust, and handy tool that Tailwind Traders can use to process all of its stores' sales every night. In the next section, we'll review what you learned and a few tips to remember.

Got stuck?
----------

If you got stuck during this exercise, here's the full code for this project.

    const fs = require("fs").promises;
    const path = require("path");
    
    async function calculateSalesTotal(salesFiles) {
      let salesTotal = 0;
      
      for (file of salesFiles) {
        
        const data = JSON.parse(await fs.readFile(file));
        
        salesTotal += data.total;
      }
      return salesTotal;
    }
    
    async function findSalesFiles(folderName) {
      
      let salesFiles = [];
    
      async function findFiles(folderName) {
        
        const items = await fs.readdir(folderName, { withFileTypes: true });
    
        
        for (item of items) {
          
          if (item.isDirectory()) {
            
            await findFiles(path.join(folderName, item.name));
          } else {
            
            if (path.extname(item.name) === ".json") {
              
              salesFiles.push(path.join(folderName, item.name));
            }
          }
        }
      }
    
      await findFiles(folderName);
    
      return salesFiles;
    }
    
    async function main() {
      const salesDir = path.join(__dirname, "stores");
      const salesTotalsDir = path.join(__dirname, "salesTotals");
    
      
      try {
        await fs.mkdir(salesTotalsDir);
      } catch {
        console.log(`${salesTotalsDir} already exists.`);
      }
    
      
      const salesFiles = await findSalesFiles(salesDir);
    
      
      const salesTotal = await calculateSalesTotal(salesFiles);
    
      
      await fs.writeFile(
        path.join(salesTotalsDir, "totals.txt"),
        `${salesTotal}\r\n`,
        { flag: "a" }
      );
      console.log(`Wrote sales totals to ${salesTotalsDir}`);
    }
    
    main();
    

Need help? See our [troubleshooting guide](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.9-exercise-read-write-files&documentId=aa84ed07-c0b8-d9ca-38a2-b3ca72422a80&versionIndependentDocumentId=080d9bf5-0a7b-57ac-30c6-2c42ca8713d8&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F9-exercise-read-write-files.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F9-exercise-read-write-files&author=buhollan) or provide specific feedback by [reporting an issue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.9-exercise-read-write-files&documentId=aa84ed07-c0b8-d9ca-38a2-b3ca72422a80&versionIndependentDocumentId=080d9bf5-0a7b-57ac-30c6-2c42ca8713d8&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F9-exercise-read-write-files.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F9-exercise-read-write-files&author=buhollan#report-feedback).


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/9-exercise-read-write-files)