# Exercise - Create files and directories - Learn

> Exercise - Create files and directories

*   5 minutes

The _fs_ module in Node.js lets you create new files and directories programmatically.

So far, you've created a robust command-line application in Node.js that can read any folder structure to find files with a .json extension. You'll need to read those files to summarize the data in them. You'll then write the totals to a new file in a new directory called _salesTotals_.

In this exercise, you'll create the _salesTotals_ directory and _totals.txt_ file where the sales totals will eventually go.

Create the salesTotals directory
--------------------------------

1.  In the `main` function, create a variable called `salesTotalsDir`, which holds the path of the _salesTotals_ directory.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
          const salesTotalsDir = path.join(__dirname, "salesTotals");
        
          
          const salesFiles = await findSalesFiles(salesDir);
        }
        
    
2.  In the `main` function, add code to create the directory if it doesn't already exist.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
          const salesTotalsDir = path.join(__dirname, "salesTotals");
        
          
          try {
            await fs.mkdir(salesTotalsDir);
          } catch {
            console.log(`${salesTotalsDir} already exists.`);
          }
        
          
          const salesFiles = await findSalesFiles(salesDir);
        }
        
    

Write the totals.txt file
-------------------------

1.  In the `main` function, add the code to create an empty file called _totals.txt_ inside the newly created _salesTotals_ directory.
    
        async function main() {
          const salesDir = path.join(__dirname, "stores");
          const salesTotalsDir = path.join(__dirname, "salesTotals");
        
          
          try {
            await fs.mkdir(salesTotalsDir);
          } catch {
            console.log(`${salesTotalsDir} already exists.`);
          }
        
          
          const salesFiles = await findSalesFiles(salesDir);
        
          
          await fs.writeFile(path.join(salesTotalsDir, "totals.txt"), String());
        }
        
    
2.  Run the program by entering the following code from the terminal prompt.
    
        node index.js
        
    
3.  Select the **Refresh** icon in the **Files** explorer.
    
     ![Screenshot of the Refresh icon in the Files explorer of the Cloud Shell editor.](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/advocates/nodejs-files/media/refresh-file-explorer.png) 
    

You're almost finished. The last step is to read the sales files, add up the totals, and write the grand total to the new _totals.txt_ file. Next you'll learn how to read and parse data inside files.

Got stuck?
----------

If you got stuck during this exercise, here's the full code up to this point.

    const fs = require("fs").promises;
    const path = require("path");
    
    async function findSalesFiles(folderName) {
      
      let salesFiles = [];
    
      async function findFiles(folderName) {
        
        const items = await fs.readdir(folderName, { withFileTypes: true });
    
        
        for (item of items) {
          
          if (item.isDirectory()) {
            
            await findFiles(path.join(folderName, item.name));
          } else {
            
            if (path.extname(item.name) === ".json") {
              
              salesFiles.push(path.join(folderName, item.name));
            }
          }
        }
      }
    
      await findFiles(folderName);
    
      return salesFiles;
    }
    
    async function main() {
      const salesDir = path.join(__dirname, "stores");
      const salesTotalsDir = path.join(__dirname, "salesTotals");
    
      
      try {
        await fs.mkdir(salesTotalsDir);
      } catch {
        console.log(`${salesTotalsDir} already exists.`);
      }
    
      
      const salesFiles = await findSalesFiles(salesDir);
    
      
      await fs.writeFile(path.join(salesTotalsDir, "totals.txt"), String());
      console.log(`Wrote sales totals to ${salesTotalsDir}`);
    }
    
    main();
    

* * *

Next unit: Read and write to files
----------------------------------

[Continue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/modules/nodejs-files/8-read-write-files/)

Need help? See our [troubleshooting guide](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.7-exercise-create-files-directories&documentId=1b7ffbc5-2ac3-ba73-68e6-6464220ed971&versionIndependentDocumentId=017fde35-ba32-6843-191b-797309100585&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F7-exercise-create-files-directories.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F7-exercise-create-files-directories&author=buhollan) or provide specific feedback by [reporting an issue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.7-exercise-create-files-directories&documentId=1b7ffbc5-2ac3-ba73-68e6-6464220ed971&versionIndependentDocumentId=017fde35-ba32-6843-191b-797309100585&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F7-exercise-create-files-directories.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F7-exercise-create-files-directories&author=buhollan#report-feedback).


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/7-exercise-create-files-directories)