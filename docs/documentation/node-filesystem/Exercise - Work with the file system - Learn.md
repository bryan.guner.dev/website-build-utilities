# Exercise - Work with the file system - Learn

> Use the "fs" module to read files and folders on the file system.

*   7 minutes

You can use Node.js to find and return information about files and folders.

Tailwind Traders has many physical stores all over the world. Each night, these stores create a file called _sales.json_ that contains the total for all their sales for the previous day. These files are organized in folders by store ID.

In this exercise, you'll write a Node.js program that can search for files called _sales.json_ in a folder.

Sign in to the sandbox
----------------------

Activate the Microsoft Learn sandbox by selecting **Activate Sandbox** at the top of this page.

Set up the environment
----------------------

Run the following command in Azure Cloud Shell on the right to ensure that you're working with the most current version of Node.js:

    source <(curl -Ls https://aka.ms/install-node-lts)
    

Clone the project
-----------------

1.  Run the following command to clone the example project for this module:
    
        git clone https://github.com/MicrosoftDocs/node-essentials && cd node-essentials/nodejs-files
        
    
2.  Open the Cloud Shell editor by typing the following command in Cloud Shell and selecting the Enter key.
    
        code .
        
    
3.  Expand the _stores_ folder and each of the numbered folders inside.
    
     ![Screenshot that shows the project folder structure.](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/advocates/nodejs-files/media/folder-structure.png) 
    

Find the sales.json files
-------------------------

You need to find all the files in only the topmost location: the _stores_ folder.

### Include the fs module

1.  Select the _index.js_ file to open it in the editor.
    
2.  Include the _fs_ module at the top of the file.
    
        const fs = require("fs").promises;
        
    
3.  Create a `main` method. This method will be the entry point for your code. The last line of code in this file will invoke the `main` method.
    
        const fs = require("fs").promises;
        
        async function main() {}
        
        main();
        
    

### Write a method to find the sales.json files

1.  Create a new method called `findSalesFiles` that takes a `folderName` parameter.
    
        async function findSalesFiles(folderName) {
          
        }
        
    
2.  Add an array at the top, which will hold the paths to all the sales files that the program finds.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          
        }
        
    
3.  Create a method within this function called `findFiles`, which also takes a `folderName` parameter.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          async function findFiles(folderName) {
            
          }
        }
        
    
    This new method `findFiles` is created inside the main `findSalesMethod` method so that it can run as many times as necessary to find all the sales files and populate the `salesFiles` array. The `folderName` value is the path to the current folder.
    
4.  Inside the `findFiles` method, read the `currentFolder` path with the `readdirsync` method.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          async function findFiles(folderName) {
            
            const items = await fs.readdir(folderName, { withFileTypes: true });
        
            
          }
        }
        
    
5.  Add a block to loop over each item returned from the `readdirsync` method.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          async function findFiles(folderName) {
            
            const items = await fs.readdir(folderName, { withFileTypes: true });
        
            
            for (item of items) {
              
            }
          }
        }
        
    
6.  Add an `if` statement to determine if the item is a file or a directory.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          async function findFiles(folderName) {
            
            const items = await fs.readdir(folderName, { withFileTypes: true });
        
            
            for (item of items) {
              if (item.isDirectory()) {
                
              } else {
                
              }
            }
          }
        }
        
    
7.  If the item is a directory, call the `findFiles` method again, passing in the path to the item. If it's not, add a check to make sure the item name matches _sales.json_.
    
        async function findSalesFiles(folderName) {
          
          let salesFiles = [];
        
          async function findFiles(folderName) {
            
            const items = await fs.readdir(folderName, { withFileTypes: true });
        
            
            for (item of items) {
              if (item.isDirectory()) {
                
                await findFiles(`${folderName}/${item.name}`);
              } else {
                
                if (item.name === "sales.json") {
                  
                  salesFiles.push(`${folderName}/${item.name}`);
                }
              }
            }
          }
          await findFiles(folderName);
          return salesFiles;
        }
        
    
8.  Call this new `findSaleFiles` function from the `main` method. Pass in the _stores_ folder name as the location to search for files.
    
        async function main() {
          const salesFiles = await findSalesFiles("stores");
          console.log(salesFiles);
        }
        
    

Run the program
---------------

1.  Enter the following command into Cloud Shell to run the program.
    
        node index.js
        
    
2.  The program should show the following output.
    
        [
         'stores/201/sales.json',
         'stores/202/sales.json',
         'stores/203/sales.json',
         'stores/204/sales.json',
        ]
        
    

Excellent! You've successfully written a command-line program that will traverse any directory and find all the _sales.json_ files inside.

However, the way that the path to subfolders was constructed in this example is a little clumsy because it requires concatenating strings together. Also, you might run into issues on other operating systems (like Windows) that use different path separators.

In the next section, you'll learn how to construct paths that work across operating systems by using the _path_ module.

### Got stuck?

If you got stuck at any point in this exercise, here's the completed code. Remove everything in _index.js_ and replace it with this solution.

    const fs = require("fs");
    
    async function findSalesFiles(folderName) {
      
      let salesFiles = [];
    
      async function findFiles(folderName) {
        
        const items = await fs.readdirSync(folderName, { withFileTypes: true });
    
        
        for (item of items) {
          
          if (item.isDirectory()) {
            
            await findFiles(`${folderName}/${item.name}`);
          } else {
            
            if (item.name === "sales.json") {
              
              salesFiles.push(`${folderName}/${item.name}`);
            }
          }
        }
      }
    
      
      await findFiles(folderName);
    
      
      return salesFiles;
    }
    
    async function main() {
      const salesFiles = await findSalesFiles("stores");
      console.log(salesFiles);
    }
    
    main();
    

* * *

Next unit: Work with file paths in Node.js
------------------------------------------

[Continue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/modules/nodejs-files/4-paths/)

Need help? See our [troubleshooting guide](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.3-exercise-file-system&documentId=12f33a70-107c-1192-980b-b455a044dd3a&versionIndependentDocumentId=b0bcd963-9662-5142-9412-349b4e7490dd&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F3-exercise-file-system.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F3-exercise-file-system&author=buhollan) or provide specific feedback by [reporting an issue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/support/troubleshooting?uid=learn.azure.nodejs-files.3-exercise-file-system&documentId=12f33a70-107c-1192-980b-b455a044dd3a&versionIndependentDocumentId=b0bcd963-9662-5142-9412-349b4e7490dd&contentPath=%2FMicrosoftDocs%2Flearn-pr%2Fblob%2Flive%2Flearn-pr%2Fadvocates%2Fnodejs-files%2F3-exercise-file-system.yml&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Flearn%2Fmodules%2Fnodejs-files%2F3-exercise-file-system&author=buhollan#report-feedback).


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/3-exercise-file-system)