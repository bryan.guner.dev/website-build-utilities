# Work with file paths in Node.js - Learn

> Work with file paths in Node.js

*   4 minutes

Node.js has a built-in mechanism for working with file system paths.

In the previous exercise, we didn't have many folders to find and look through. If you have many files or folders, manually building paths can get tedious. Node.js provides some built-in constants and utilities to make file paths easier.

Here, you'll learn about the Node.js _path_ module and the `__dirname` constant so that you can make the program smarter and more resilient.

Determine the current directory
-------------------------------

Sometimes, you don't know the directory where your program will run. You need it to just use the path of the current directory. Node.js exposes the full path to the current directory via the constant `__dirname`.

    console.log(__dirname);
    

If you run that code from the _sales_ folder in the following folder structure, the `_dirname` value will be `/stores/201/sales`.

    📂 stores
        📂 201
            📂 sales
    

Work with paths
---------------

Paths are a subject that comes up so frequently that Node.js includes a module called _path_ specifically for working with paths.

Just like the _fs_ module, the _path_ module ships with Node.js and doesn't need to be installed. You just need to reference it at the top of your file.

    const path = require("path");
    

### Join paths

The _path_ module works with the concept of file and folder paths, which are just strings. For instance, if you want to get the path to the _stores/201_ folder, you can use the _path_ module to do that.

    console.log(path.join("stores", "201")); 
    

The reason why you would use the _path_ module instead of concatenating strings is that your program might run on Windows or Linux. The _path_ module will always format paths correctly for whatever operating system it's running on. In the preceding example, `path.join` would return `stores\201` on Windows, with a backslash instead of a slash.

### Determine file name extensions

The _path_ module can also tell you what the extension of a file name is. If you have a file and you want to identify if it's a JSON file or not, you can use the `path.extname` method.

    console.log(path.extname("sales.json"));
    

Tip

The _path_ module doesn't care whether or not things actually exist. Paths are conceptual, not physical. It's just building and parsing strings for you.

### Get everything you need to know about a file or path

The _path_ module contains many different methods that do various things. However, you can get most of the information that you need about a path or file by using the `parse` method. This method returns an object that contains the current directory you're in, the name of the file, the file name extension, and even the name of the file without the extension.

    console.log(path.parse("stores/201/sales.json"));
    
    

There are many more useful utility methods on the _path_ module, but these are the core concepts that you'll likely use most often. In the next exercise, you'll use the _path_ module to compose paths and identify .json files.

* * *

Next unit: Exercise - Work with paths
-------------------------------------

[Continue](chrome-extension://cjedbglnccaioiolemnfhjncicchinao/en-us/learn/modules/nodejs-files/5-exercise-paths/)


[Source](https://docs.microsoft.com/en-us/learn/modules/nodejs-files/4-paths)